<?php
/**
 * Plugin Name: JF WebDesign Disable Pingback
 * Plugin URI: https://www.jfwebdesign.com/
 * Description: This is a drop in plugin to disable pingback globally. Pingback can be responsible for your site being used for malicious attacks.
 * Version: 1.1
 * Author: JF WebDesign
 * Author URI: https://www.jfwebdesign.com/
 */
// Code source: Sucuri (http://blog.sucuri.net/2014/03/more-than-162000-wordpress-sites-used-for-distributed-denial-of-service-attack.html)
add_filter( 'xmlrpc_methods', function( $methods ) {
	unset( $methods['pingback.ping'] );
	return $methods;
} );
?>